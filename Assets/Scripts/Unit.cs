﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

    float speed = 10;
    Vector3[] path;
    int targetIndex;

    public GameObject graphics;
    public Animator anim;

    private GameObject inventoryPanel;

    void Awake()
    {
        anim = graphics.GetComponent<Animator>();
        inventoryPanel = GameObject.Find("Inventory Panel");
    }


    void Update()
    {
        bool RMB = Input.GetMouseButtonDown(1);

        if (RMB)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit) && hit.transform.tag == "Ground")
            {
                anim.SetBool("isMoving", true);
                targetIndex = 0;
                PathRequestManager.RequestPath(transform.position, hit.point, OnPathFound);
            }
        }

        CallInventory();

    }

    private void CallInventory()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            if (inventoryPanel.activeSelf)
                inventoryPanel.SetActive(false);
            else
                inventoryPanel.SetActive(true);
        }
    }

    public void OnPathFound(Vector3[] newPath, bool pathSuccessfull)
    {
        if (pathSuccessfull)
        {
            path = newPath;
            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath");
        }
    }

    IEnumerator FollowPath()
    {
        Vector3 currentWaypoint = path[0];
        Debug.Log(currentWaypoint);
        while (true)
        {
            if (transform.position == currentWaypoint)
            {
                targetIndex++;
                Debug.Log("target index: " + targetIndex);
                Debug.Log("path length: " + path.Length);
                if (targetIndex >= path.Length)
                {
                    anim.SetBool("isMoving", false);
                    Debug.Log("stopping coroutine because target index is greater than path length");
                    yield break;
                }
                currentWaypoint = path[targetIndex];
                Debug.Log("next waypoint: " + currentWaypoint);
            }
            transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
            Quaternion transRot = Quaternion.LookRotation(currentWaypoint - this.transform.position, Vector3.up);
            graphics.transform.rotation = Quaternion.Slerp(transRot, graphics.transform.localRotation, 0.7f);
            
            yield return null;
        }
    }

    public void OnDrawGizmos()
    {
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], Vector3.one);

                if (i == targetIndex)
                {
                    Gizmos.DrawLine(transform.position, path[i]);
                }
                else
                {
                    Gizmos.DrawLine(path[i - 1], path[i]);
                }
            }
        }
    }
}
