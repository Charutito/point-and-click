﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Transform cameraTarget;
    
    public float distance;
    public float height;
    public float heightDamping;
    
    float rotate;
    Vector3 offset;
    public float rotateSpeed;

    Vector3 lastPosition;
    public float smoothing;

    void Start()
    {
        lastPosition = new Vector3(cameraTarget.transform.position.x, cameraTarget.transform.position.y + height, cameraTarget.transform.position.z - distance);
        offset = new Vector3(cameraTarget.transform.position.x, cameraTarget.transform.position.y + height, cameraTarget.transform.position.z - distance);
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            rotate = -1;
        }
        else if (Input.GetKey(KeyCode.E))
        {
            rotate = 1;
        }
        else
        {
            rotate = 0;
        }

    }

    void LateUpdate()
    {
        if (!cameraTarget)
            return;

        offset = Quaternion.AngleAxis(rotate * rotateSpeed, Vector3.up) * offset;
        //transform.position = cameraTarget.transform.position + offset;




        float wantedHeight = cameraTarget.position.y + height;
        float currentHeight = transform.position.y;
        currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);
        transform.position = new Vector3(cameraTarget.position.x, currentHeight, cameraTarget.position.z);
        transform.position -= Vector3.forward * distance;
        

        lastPosition = transform.position;

        transform.LookAt(cameraTarget);
    }
}